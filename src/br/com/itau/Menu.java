package br.com.itau;

import javax.xml.bind.SchemaOutputResolver;

public class Menu {

    public Menu() {

        Baralho baralho = new Baralho(1);
        baralho.setCartas(baralho.novoBaralho());

        boolean continuarJogo = true;
        int posicao = 0;
        int pontos = 0;

        IO.imprimirMensagemInicial();

        while (continuarJogo) {

            IO.imprimirOpcoes();

            String opcao = IO.lerOpcao();

            if (opcao.equalsIgnoreCase("S")) {
                Carta carta = new Carta(
                        baralho.retirarCarta(baralho.getCartas(), posicao).getNumero(),
                        baralho.retirarCarta(baralho.getCartas(), posicao).getNaipe());

                carta.imprimirCarta(carta);

                pontos = pontos + verificarPontuacaoCarta(carta.getNumero());

                if (verificarPontuacao(pontos)) {
                    posicao++;
                } else {
                    continuarJogo = false;
                }

                if (!continuarJogo) {
                    IO.imprimirResultado(pontos);
                }

            } else if (opcao.equalsIgnoreCase("N")) {
                IO.imprimirResultado(pontos);
                continuarJogo=false;

            } else {
                System.out.println("Opção inválida! Responda com 'S' ou 'N'.");
                System.out.println("");
            }
        }

    }

    private int verificarPontuacaoCarta(int numero) {
        if (numero == 11 || numero == 12 || numero == 13) {
            return 10;
        } else {
            return numero;

        }
    }

    private boolean verificarPontuacao(int pontos) {
        if (pontos < 21) {
            return true;
        } else {
            return false;
        }
    }

}

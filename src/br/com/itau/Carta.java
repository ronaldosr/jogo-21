package br.com.itau;

public class Carta {

    private int numero;
    private String naipe;

    public Carta(int carta, String naipe) {
        this.numero = carta;
        this.naipe = naipe;
    }

    public int getNumero() {
        return numero;
    }

    public String getNaipe() {
        return naipe;
    }

    public void imprimirCarta(Carta carta) {
        IO.imprimirCarta(carta);
    }
}


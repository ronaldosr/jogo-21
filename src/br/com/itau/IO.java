package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {

    public static void imprimirMensagemInicial() {
        System.out.println("*** Jogo 21 ***");
        System.out.println(
                "\n1 ponto para o Ás \n2 a 10 pontos para as cartas numéricas \n10 pontos para Valete, Rainha e Rei\n " +
                "\nVocê vence se fizer 21 pontos" +
                "\nVocê perde se fizer mais de 21 pontos" +
                "\nVocê perde se desistir com menos de 21 pontos \n");
    }


    public static String lerOpcao() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public static void imprimirOpcoes() {
        System.out.println("Deseja selecionar uma carta?");
        System.out.println("S - Sim");
        System.out.println("N - Não");
    }

    public static void imprimirCarta(Carta carta) {
        if (carta.getNumero() == 11) {
            System.out.println("Valete de " + carta.getNaipe());
        } else if (carta.getNumero() == 12) {
            System.out.println("Rainha de " + carta.getNaipe());
        } else if(carta.getNumero() == 13) {
            System.out.println("Rei de " + carta.getNaipe());
        } else if (carta.getNumero() == 1) {
            System.out.println("Ás de " + carta.getNaipe());
        } else {
            System.out.println(carta.getNumero() + " de " + carta.getNaipe());
        }
    }

    public static void imprimirResultado(int pontos) {
        if (pontos == 21) {
            System.out.println("Parabéns, você fez 21 pontos! \n");
        } else if (pontos > 21) {
            System.out.println("Você perdeu! Total de " + pontos + " pontos. \n");
        } else {
            System.out.println("Você desistiu com " + pontos + " pontos. Seu mole!\n");
        }
    }

    public static boolean novoJogo() {
        System.out.println("Pressione qualquer tecla para jogar novamente ou pressine 'N' para sair" );
        System.out.println("");
        if (lerOpcao().equalsIgnoreCase("N")){
            return false;
        } else
            return true;
    }
}

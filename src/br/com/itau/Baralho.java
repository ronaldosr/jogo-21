package br.com.itau;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Baralho {
    private int id;
    private List<Carta> cartas;

    public Baralho(int id) {
        this.id = id;
    }

    // Cria um novo baralho
    public List<Carta> novoBaralho () {

        List<Carta> cartas = new ArrayList<>();

        // Adiciona cada naipe ao baralho
        cartas.addAll(criarNaipeEspadas("Espadas"));
        cartas.addAll(criarNaipePaus("Paus"));
        cartas.addAll(criarNaipePaus("Copas"));
        cartas.addAll(criarNaipeOuro("Ouro"));

        return embaralhaCartas(cartas);
    }

    // Embaralha as cartas
    public List<Carta> embaralhaCartas(List<Carta> cartas) {
        Collections.shuffle(cartas);
        return cartas;
    }

    // Retirar uma carta do baralho
    public Carta retirarCarta(List<Carta> cartas, int posicao){
        return cartas.get(posicao);
    }

    // Cria o naipe de espadas
    public List<Carta> criarNaipeEspadas(String espadas) {
        List<Carta> cartas = new ArrayList<>();

        for (int i = 1; i <= 13; i++) {
            Carta carta = new Carta(i, espadas);
            cartas.add(carta);
        }
        return cartas;
    }

    // Cria o naipe de paus
    private List<Carta> criarNaipePaus(String paus) {
        List<Carta> cartas = new ArrayList<>();

        for (int i = 1; i <= 13; i++) {
            Carta carta = new Carta(i, paus);
            cartas.add(carta);
        }
        return cartas;
    }

    // Cria o naipe de copas
    private List<Carta> criarNaipeCopas (String copas) {
        List<Carta> cartas = new ArrayList<>();

        for (int i = 1; i <= 13; i++) {
            Carta carta = new Carta(i, copas);
            cartas.add(carta);
        }
        return cartas;
    }

    // Cria o naipe de ouro
    private List<Carta> criarNaipeOuro(String ouro) {
        List<Carta> cartas = new ArrayList<>();

        for (int i = 1; i <= 13; i++) {
            Carta carta = new Carta(i, ouro);
            cartas.add(carta);
        }
        return cartas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Carta> getCartas() {
        return cartas;
    }

    public void setCartas(List<Carta> cartas) {
        this.cartas = cartas;
    }
}
